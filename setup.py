from setuptools import setup, find_packages

setup(
    name='alumnosipc',
    version='0.0.1',
    install_requires=[
        'requests',
        'importlib-metadata; python_version<"3.10"',
        'gspread >= 5.10.0',
        'pandas >= 1.5.3',
    ],
    packages=["alumnosipc"],
    package_dir={"": "."},
    package_data={"": ["*.json"]},
    include_package_data=True
)

