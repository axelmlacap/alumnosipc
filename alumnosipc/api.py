import pandas as pd
from pathlib import Path
import gspread
import typing as tp

print(__file__)
f = Path(__file__).parent
print(list(f.iterdir()))
_CRED = f / "alumnosipc.json"
_SERVER = "https://docs.google.com/spreadsheets/d/19D_h5OCis0_T11JYD5LIhB5VnFVKPXmMLe0qUx344r8"
_COURSEPROPS = {"año": 2024,
                "semestre": 1,
                "comisión": "Lacapmesure",
                "materia": "i100 Introducción al Pensamiento Computacional"}


class InvalidUser(Exception):
	pass


class InvalidPassword(Exception):
	pass


class APISesion:
	_actions = {"1": ("Imprimir información del alumno.", "printStudentInfo"),
	            "2": ("Imprimir notas del alumno.", "printStudentGrades")}
	_defaults = {"paths": {"server": None,
	                       "cred": None},
	             "sheetNames": {"grades": "Calificaciones",
	                            "attendance": "Asistencia",
	                            "students": "Alumnos",
	                            "keywords": "Palabras clave"},
	             "usrColName": "Correo electrónico",
	             "pwdColName": "DNI",
	             "maxStudentCols": 3,
	             "grades": {"TP1": "Trabajo Práctico 1",
	                        "TP2": "Trabajo Práctico 2",
	                        "TP3": "Trabajo Práctico 3",
	                        "p1": "Parcialito 1",
	                        "P": "Parcial",
	                        "p2": "Parcialito 2",
	                        "GP*": "Grupo de Parciales",
	                        "F*": "Final",
	                        "RF": "Recuperatorio",
	                        "NdC*": "Nota de Cursada"}}
	_allowNewPaths = False
	_username = None

	def __init__(self):
		self._paths = {}
		self._client = None
		self._sheetNames = dict(self._defaults["sheetNames"])
		self._usrColName = self._defaults["usrColName"]
		self._pwdColName = self._defaults["pwdColName"]
		self._maxStudentCols = self._defaults["maxStudentCols"]
		self._grades = dict(self._defaults["grades"])

		self._studentInfo = None
		self._studentGrades = None

	@property
	def studentInfo(self) -> tp.Dict[str, tp.Any]:
		if self._studentInfo is None:
			self._getStudentInfo()

		return self._studentInfo

	@property
	def studentGrades(self) -> tp.Dict[str, str]:
		if self._studentGrades is None:
			self._getStudentGrades()

		return self._studentGrades

	@property
	def paths(self) -> tp.Dict[str, tp.Union[Path, None]]:
		return self._paths

	@paths.setter
	def paths(self, value: tp.Dict[str, tp.Union[str, Path, None]]):
		for which, path in value.items():
			self.setPath(which, path)

	@property
	def sheetNames(self) -> tp.Dict[str, str]:
		return self._sheetNames

	@property
	def client(self) -> gspread.Client:
		if self._client is None:
			self._authenticate()

		return self._client

	def _authenticate(self, cred: tp.Union[str, Path] = None) -> gspread.Client:
		global _SERVER

		if cred is not None:
			_SERVER = cred
		if _SERVER is None:
			raise IOError(f"Error al autenticar API de Google Drive: no se especificó ninguna credencial.")
		path = Path(_CRED).resolve()
		self._client = gspread.service_account(path)

		return self._client

	def _resetBuffer(self):
		self._studentInfo = None
		self._studentGrades = None

	def _getSheet(self, sheetName: str = None, index: str = None, *args, **kwargs):
		file = self.client.open_by_url(_SERVER)
		if sheetName is None:
			worksheet = file.get_worksheet(0)
		else:
			worksheet = file.worksheet(sheetName)
		sheet = pd.DataFrame(worksheet.get_all_records(*args, **kwargs))

		if index is not None:
			sheet.index = sheet[index]
			sheet = sheet.sort_index()

		return sheet

	def _getStudents(self) -> pd.DataFrame:
		return self._getSheet(self.sheetNames["students"], self._usrColName, value_render_option="UNFORMATTED_VALUE")

	def _getGrades(self) -> pd.DataFrame:
		return self._getSheet(self.sheetNames["grades"], self._usrColName, value_render_option="UNFORMATTED_VALUE")

	# def setPath(self, which: str, path: tp.Union[str, Path, None], passThrough: bool = False) -> tp.Union[Path, None]:
	# 	# Verifico si es un tipo de ruta permitido/existente
	# 	if not self._allowNewPaths and which not in self.paths:
	# 		return None
	#
	# 	# Convierto tipo de dato
	# 	if path is None:
	# 		pass
	# 	elif validators.url(str(path)):
	# 		path = str(path)
	# 	else:
	# 		path = Path(path).resolve()
	#
	# 	# Verifico si la ruta se modifica
	# 	if which in self.paths and path == self.paths[which]:
	# 		return self.paths[which]
	#
	# 	# Valido el valor
	# 	if which == "cred":
	# 		if not path.exists():
	# 			raise ValueError(f"Error asignando la ruta {which}: no existe el archivo '{path}'.")
	# 		if not path.suffix.lower() == ".json":
	# 			raise ValueError(
	# 				f"Error asignando la ruta '{which}': el archivo debe corresponder a una credencial "
	# 				f"de Google API con extensión '.json'. Se recibió: '{path}'.")
	# 	elif which in ["students", "output", "input", "campus", "gestion", "keywords"]:
	# 		if isinstance(path, Path) and path.suffix.lower() not in [".csv", ".xls", ".xlsx"]:
	# 			raise ValueError(f"Error asignando la ruta '{which}': el archivo debe ser formato '.csv', '.xls' ó "
	# 			                 f"'.xlsx'. Se recibió: '{path}'.")
	#
	# 	# Actualizo el valor
	# 	if not passThrough:
	# 		self.paths[which] = path
	#
	# 	return path

	def getUser(self):
		self._username = None
		while self._username is None:
			username = input(f"Ingrese su usuario (email sin @udesa.edu.ar): ")
			if username == "u":
				print("\n".join(self.getUserList()))
				continue
			password = input(f"Ingrese su contraseña (DNI sin puntos): ")
			result, err = self.validateUser(username, password)
			if isinstance(err, InvalidUser):
				print(f"Nombre de usuario inválido. Ingrese 'u' si desea ver los usuarios del curso.")

		self._getStudentInfo()
		print(f"\n¡Bienvenidx {self._studentInfo['Nombre']}!")

		return username

	def validateUser(self, username: str, password: str):
		global _SERVER

		username = username.strip() + "@udesa.edu.ar"
		students = self._getSheet(self.sheetNames["students"], self._usrColName, value_render_option="UNFORMATTED_VALUE")

		self._username = None
		self._resetBuffer()
		if not (username in students.index):
			return False, InvalidUser()
		if not (password == str(students.loc[username, self._pwdColName])):
			return False, InvalidPassword()
		self._username = username

		return True, None

	def getUserList(self) -> tp.List[str]:
		return ["- " + v.replace("@udesa.edu.ar", "") for v in self._getStudents()[self._usrColName]]

	def _getStudentInfo(self) -> dict:
		self._studentInfo = dict(self._getStudents().loc[self._username])

		return self._studentInfo

	def getAction(self, actions: dict = None) -> str:
		if actions is None:
			actions = dict(self._actions)
			actions["."] = ("Repetir opciones.", None)
			actions["x"] = ("Salir.", None)
		else:
			actions["."] = ("Repetir opciones.", None)
			actions["x"] = ("Volver al menú anterior.", None)

		while True:
			print("\n" + "\n".join([f"- {k}: {d}" for k, (d, a) in actions.items()]))
			key = input(f"Ingrese una opción: ")
			if key == "x":
				return "x"
			elif key == ".":
				continue
			elif key not in actions:
				print(f"Error: la opción ingresada no es válida.")
			else:
				action = actions[key][1]
				if isinstance(action, dict):
					self.getAction(action)
				elif isinstance(action, str):
					self.__getattribute__(action)()

	def printStudentInfo(self) -> str:
		header = f"\nDatos del usuario {self._username}:\n"
		content = "\n".join([f"- {k}: {v}" for k, v in self.studentInfo.items()])
		print(header + content)

		return header + content

	def _getStudentGrades(self) -> pd.DataFrame:
		df = self._getGrades()
		grades = {gradeName: "" for gradeName in self._grades.values()}
		for colName, gradeName in self._grades.items():
			if colName in df:
				grades[gradeName] = df.loc[self._username, colName]

		self._studentGrades = grades

		return grades

	def printStudentGrades(self) -> str:
		header = f"\nNotas del alumno {self._username}:\n"
		content = "\n".join([f"- {k}: {v}" for k, v in self.studentGrades.items()])
		print(header + content)

		return header + content

	def start(self):
		print(f"Bienvenidos a la plataforma de Alumnos de")
		print(_COURSEPROPS['materia'])
		print(f"Año {_COURSEPROPS['año']}, semestre {_COURSEPROPS['semestre']}, comisión {_COURSEPROPS['comisión']}")
		print()

		self.getUser()
		self.getAction()
